FROM  maven:3.8.2-jdk-11 AS MAVEN_BUILD

MAINTAINER Brian Hannaway

COPY pom.xml /build/
COPY src /build/src/

WORKDIR /build/
RUN mvn package

FROM adoptopenjdk/openjdk11:alpine-jre

RUN apk update && \
    apk add mysql-client

WORKDIR /app

RUN mkdir sql

RUN mkdir logs

COPY /db/* /app/sql/

RUN chmod 777 /app/sql
RUN chmod 777 /app/logs

RUN addgroup -S spring && adduser -S spring -G spring

USER spring:spring

ARG JAR_FILE=target/*.jar

COPY --from=MAVEN_BUILD /build/target/*.jar /app/app.jar

ENTRYPOINT ["java","-jar","app.jar"]