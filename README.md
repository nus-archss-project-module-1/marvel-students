# marvel-students

### pre steps on windows
	cd /mnt
	cd c

https://learnk8s.io/spring-boot-kubernetes-guide
/mnt/c/WorkSpace/K8POC

	• docker build -t marvel-students .
	• docker images
	• docker ps
	• docker run \  --name=marvel-students \ -p 8085:8085  \ marvel-students
	• docker login
	• docker tag marvel-students veenu143/marvel-students:1.0.0
	• docker push veenu143/marvel-students:1.0.0
	• docker run \
      --name=marvel-students-v2 \
      -p 8085:8085  \
      veenu143/marvel-students:1.0.0
	• docker stop marvel-students-v2

Kubernetes
https://kubernetes.io/docs/reference/kubectl/cheatsheet/
	• kubectl cluster-info
	• kubectl apply -f kube
	• kubectl get pods --watch
	• kubectl scale --replicas=2 deployment/marvel-student
	• kubectl get pods -l app=marvel-student --watch
	• kubectl apply -f kube/marvel-student-service.yaml
	• kubectl get services --watch
	kubectl delete -f .


## Docker Build
     docker build -t marvel-students-latest .
     docker tag marvel-students-latest veenu143/marvel-students:latest
     docker push veenu143/marvel-students:latest

## Start Kubernetes deployment
    kubectl apply -f kube/mysql/
    kubectl apply -f kube/student-service/
    kubectl apply -f kube/deploy/
    
## Delete Kubernetes Deployment
    kubectl delete -f kube/deploy/
    kubectl delete -f kube/student-service/
    kubectl delete -f kube/mysql/	
 
### AWS Installation
## awscli
	curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
	unzip awscliv2.zip
	sudo ./aws/install
    sudo apt-get install awscli

    ## iam-authenticator
    https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html
    ## eksctl
    https://docs.aws.amazon.com/eks/latest/userguide/eksctl.html



### AWS Deployment:

    aws --profile default configure set aws_access_key_id AKIAWBE54CPXMQRL3C6Z
    aws --profile default configure set aws_secret_access_key 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
    aws configure set default.region  ap-southeast-1
    aws configure set default.output json
    eksctl create cluster --name marvel-hub-eks-cluster --region  ap-southeast-1
    or
    eksctl create cluster -f cluster.yaml

	eksctl create cluster --name eks-marvel-cluster --region  ap-southeast-1 --node-type t2.micro --nodes-min 3

	aws eks --region ap-southeast-1 update-kubeconfig --name marvel-hub-eks-cluster

## Tear Down

    ## Start Kubernetes deployment
    kubectl apply -f kube/mysql/
    kubectl apply -f kube/student-service/
    
    ## Delete Kubernetes Deployment
    kubectl delete -f kube/student-service/
    kubectl delete -f kube/mysql/

    ## Tear Down AWS    
    eksctl delete cluster --name eks-marvel-cluster
    or
    eksctl delete cluster -f cluster.yaml


