drop database marvel_students_db;
create database marvel_students_db;
use marvel_students_db;

drop database marvel_courses_db;
create database marvel_courses_db;

drop
user marvel_students_rw;
CREATE
USER marvel_students_rw IDENTIFIED BY 'marvel';

GRANT ALL PRIVILEGES ON marvel_students_db.* TO
'marvel_students_rw';

drop
user marvel_courses_rw;
CREATE
USER marvel_courses_rw IDENTIFIED BY 'marvel';

GRANT ALL PRIVILEGES ON marvel_courses_db.* TO
'marvel_courses_rw';

drop
user marvel_user_rw;
CREATE
USER marvel_user_rw IDENTIFIED BY 'marvel';

GRANT ALL PRIVILEGES ON marvel_students_db.* TO
'marvel_user_rw';
GRANT ALL PRIVILEGES ON marvel_courses_db.* TO
'marvel_user_rw';

drop table student;
drop table marvel_students_db.student;
CREATE TABLE marvel_students_db.student
(
    student_id     varchar(25)  NOT NULL,
    student_name   varchar(80) ,
    student_nric   varchar(45)  ,
    parent_name    varchar(128) ,
    parent_nric    varchar(45)  ,
    contact_number varchar(128) ,
    email          varchar(50)  ,
    gender         varchar(6)   ,
    dob           varchar(10)         ,
    district       varchar(10) ,
    country        varchar(45)  ,
    address        varchar(45)  ,
    postal_code    int          ,
    interests varchar(200),
    user_password  varchar(10),
    PRIMARY KEY (student_id)
    -- ,UNIQUE KEY email_UNIQUE (email)
);

INSERT INTO marvel_students_db.student (`student_id`,`student_name`,`student_nric`,`parent_name`,`parent_nric`,`contact_number`,`email`,`gender`,`dob`,`district`,`country`,`address`,`postal_code`,`user_password`,`interests`) VALUES ('1618819554023','Robin','S243322','Hood Brother','','','r','Male','','','','',0,'r',NULL);
INSERT INTO marvel_students_db.student (`student_id`,`student_name`,`student_nric`,`parent_name`,`parent_nric`,`contact_number`,`email`,`gender`,`dob`,`district`,`country`,`address`,`postal_code`,`user_password`,`interests`) VALUES ('1619934254973','Joshua','S342342N','Derek','S2342242N','2523422','j','Male','2012-06-06','Tampines','Singapore','sdfasfd',521156,'j',NULL);
INSERT INTO marvel_students_db.student (`student_id`,`student_name`,`student_nric`,`parent_name`,`parent_nric`,`contact_number`,`email`,`gender`,`dob`,`district`,`country`,`address`,`postal_code`,`user_password`,`interests`) VALUES ('1619943421292','Nicole','S234324','Thomas','S523432','23423423','n','Male','2020-02-02','Tampines','Singapore','Tampines',234233,'n','none,Maths,Tennis,Swimming,English,Reading,Visual Arts');

drop table marvel_students_db.login_details;
CREATE TABLE marvel_students_db.login_details
(
    user_name  varchar(80) NOT NULL,
    user_password  varchar(10),
    id varchar(25) not null,
    name  varchar(80) not null,
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (user_name)
);

select * from marvel_students_db.student;
select * from marvel_students_db.login_details ;

-- -- students login
drop table marvel_students_db.login_details;
CREATE TABLE marvel_students_db.login_details
(
    user_name  varchar(80) NOT NULL,
    user_password  varchar(10),
    id varchar(25) not null,
    name  varchar(80) not null,
    created_time timestamp DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (user_name)
);
truncate table marvel_students_db.login_details;
INSERT INTO marvel_students_db.login_details (`user_name`,`user_password`,`id`,`name`,`created_time`) VALUES ('j','j','1619934254973','Joshua','2021-05-02 13:44:15');
INSERT INTO marvel_students_db.login_details (`user_name`,`user_password`,`id`,`name`,`created_time`) VALUES ('n','n','1619943421292','Nicole','2021-05-02 16:17:01');
INSERT INTO marvel_students_db.login_details (`user_name`,`user_password`,`id`,`name`,`created_time`) VALUES ('r','r','1618819554023','Robin','2021-04-19 16:05:54');
