drop database marvel_students_db;
create database marvel_students_db;
use marvel_students_db;

drop
user marvel_students_rw;
CREATE
USER marvel_students_rw IDENTIFIED BY 'marvel';

GRANT ALL PRIVILEGES ON marvel_students_db.* TO
'marvel_students_rw';


