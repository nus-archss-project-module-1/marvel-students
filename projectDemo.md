## Points
- Domain Drive Design
- Micro-Services
- Separate DB for each Services
- Containarized services
- Deployment via Kubernetis
- Deploy in AWS
- Logging  trace and span
- Dev Ops Pipeline
- Infrastructure As Code
- DB Choice
    - String relationship of entity
    - transaction properties to manage the seat inventory
- Why Kubernetis
    - Support cloud
    - Auto scaling
    - Self healing
    - One pod one service easy scale
- Agile Process
- Report in Jira DashBoard
- 

## Document
- Overview: Pradeep
   -  Project Description
   -  Platform Thinking: Producer,Consumer,Seed,Match-Making,Magnet
   -  Scope of the Project:
      - Use Cases

- Project Conduct: Pradeep
   - Current Status: What we achieved
   - Progress
      - Estimation
      - Agile Process
      - Jira Dashboard
      - Effort

- Logical Architecture & Design: Vinit
   - Domain Drive Design 
   - Architectural Decision of design
      - Why Micro-services
      - Why DDD
   - Functional Component Diagram 
   - Use Cases
   - Deployment Diagram

- Physical Architecture & Design: Pradeep
   - Tech Stack
     - Programming Language
     - Spring Boot
     - Rest Call
     - DB
     - UI Framework
     - Container
     - Kubernetis
     - Cloud: AWS
     
   - architectural decisions 
   - Physical Component Diagram 
   - ER Diagram
  
- DevOps and Development Lifecycle: Vinit
   - Version Control: GITLab
   - CI/CD: GitLab
   - Testing: Junits/Mockito
   - Image Repo: DockerHub 
   - technical findings and issues encountered: EKS integration with GitLab
   - Infrastructure As Code
   - Deployment via Kubernetis
   - Deploy in AWS
   - Logging  trace and span
 
 - Demo
    