package com.marvel.students;

import com.marvel.students.controller.StudentRegistrationController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentRegistrationController.class);

    public static void main(String[] args) {
        SpringApplication.run(StudentApp.class, args);
        LOGGER.info("Marvel Student Application is started successfully.");
    }

}
