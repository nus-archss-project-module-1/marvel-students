package com.marvel.students.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "aws")
public class AWSConfiguration {

    private String accesskeyid;
    private String seceretKeyAccess;
    private String region;
    private String kmskeyid;
    private String ssmDBKey;

    public String getEncrypted() {
        return encrypted;
    }

    public void setEncrypted(String encrypted) {
        this.encrypted = encrypted;
    }

    private String encrypted;

    public String getS3BucketName() {
        return s3BucketName;
    }

    public void setS3BucketName(String s3BucketName) {
        this.s3BucketName = s3BucketName;
    }

    public String getS3DBKey() {
        return s3DBKey;
    }

    public void setS3DBKey(String s3DBKey) {
        this.s3DBKey = s3DBKey;
    }

    private String s3BucketName;
    private String s3DBKey;

    public String getSsmDBKey() {
        return ssmDBKey;
    }

    public void setSsmDBKey(String ssmDBKey) {
        this.ssmDBKey = ssmDBKey;
    }

    public String getAccesskeyid() {
        return accesskeyid;
    }

    public void setAccesskeyid(String accesskeyid) {
        this.accesskeyid = accesskeyid;
    }

    public String getSeceretKeyAccess() {
        return seceretKeyAccess;
    }

    public void setSeceretKeyAccess(String seceretKeyAccess) {
        this.seceretKeyAccess = seceretKeyAccess;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getKmskeyid() {
        return kmskeyid;
    }

    public void setKmskeyid(String kmskeyid) {
        this.kmskeyid = kmskeyid;
    }
}
