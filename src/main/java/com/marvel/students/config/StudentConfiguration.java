package com.marvel.students.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marvel.students.service.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 5:04 PM
 */
@Configuration
public class StudentConfiguration {

    private static final String JDBC_URL = "jdbc:mysql://marvel-db-encrypted.crql4ccx4jyt.ap-southeast-1.rds.amazonaws.com:3306/marvel_students_db";

    @Autowired
    PasswordService passwordService;

    @Autowired
    AWSConfiguration awsConfiguration;

    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.username("marvel_students_rw");
        dataSourceBuilder.password(passwordService.getMarvelDBPassword());
        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
        dataSourceBuilder.url(JDBC_URL);
        System.out.println("*************Data*****"+awsConfiguration.getEncrypted());
        return dataSourceBuilder.build();
    }
}
