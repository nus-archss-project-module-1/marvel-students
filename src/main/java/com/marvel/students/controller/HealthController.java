package com.marvel.students.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("marvel-student")
public class HealthController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HealthController.class);

    @RequestMapping("/health")
    public String studentServiceHealth(@RequestHeader("marvel-student-request")String header) {
        LOGGER.info("Greetings from Marvel Student!");
        return "Greetings from Marvel Student!";
    }

    @GetMapping("/heartbit")
    public String heartBit( String header) {
        return "Heartbit Marvel Student!";
    }

    @ExceptionHandler({ MissingRequestHeaderException.class })
    public String handleException() {
        return "UnAuthorized Access to the Service";
    }
}
