package com.marvel.students.controller;

import com.marvel.students.model.LoginDetails;
import com.marvel.students.model.ResponseDto;
import com.marvel.students.model.Student;
import com.marvel.students.repository.StudentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @Author Pradeep Kumar
 * @create 4/8/2021 12:23 AM
 */
@RestController
@RequestMapping("marvel-student")
public class StudentRegistrationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentRegistrationController.class);

    @Autowired
    private StudentManager studentManager;

    @PostMapping(value = "/registerStudent", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseDto> registerStudent(@RequestHeader ("marvel-student-request") String header, @RequestBody Student student) {
        try {
            LOGGER.info("Incoming Student Registration information: {}, Request Header: {}", student, header);
            Student saved = studentManager.registerStudent(student);

            LOGGER.info(String.format("Successfully registered Student: %s", saved.getStudentName()));
            ResponseDto responseDto = getClientMessage(saved, "Successfully saved");
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            ResponseDto responseDto = getClientMessage(student, "Error occurred in the server");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(responseDto);
        }
    }

    private ResponseDto getClientMessage(Student object, String message) {
        String clientMessage = String.format("ID: [%s], Name: [%s], Status: [%s]", object.getId(), object.getEmail(), message);
        return new ResponseDto(object.getId(), object.getEmail(), clientMessage);
    }

    @PostMapping(value = "/login", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseDto> login(@RequestHeader ("marvel-student-request") String header, @RequestBody LoginDetails loginDetails) {
        try {
            LOGGER.info("Incoming Student Provider Login: {}, Request: Header: {}", loginDetails, header);

            LoginDetails user = studentManager.checkUser(loginDetails);
            if (Objects.nonNull(user)) {
                LOGGER.info(String.format("User found in database: %s", loginDetails.getUserName()));
                ResponseDto responseDto = new ResponseDto(user.getId(), user.getName(), "Success");
                return new ResponseEntity<>(responseDto, HttpStatus.OK);
            }
            LOGGER.info(String.format("User not found in database: %s", loginDetails.getUserName()));
            ResponseDto responseDto = new ResponseDto("1", "Failure", "Invalid username/password");
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Error occurred: {}", e.getMessage(), e);
            ResponseDto responseDto = new ResponseDto("1", "Name", "Exception occurred while trying to login");
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(responseDto);
        }
    }

    @GetMapping(value = "/fetchStudent/{studentId}", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public Student fetchStudent(@RequestHeader ("marvel-student-request") String header, @PathVariable String studentId) {
        try {
            LOGGER.info("Fetching my Courses. Student ID: [{}], Request Header: [{}]", studentId, header);
            Student student = studentManager.fetchStudent(studentId);
            LOGGER.info("Successfully fetched Student : [{}]", student);
            return student;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage(), e);
            return null;
        }
    }
}
