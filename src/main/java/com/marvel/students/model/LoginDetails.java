package com.marvel.students.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 4:55 PM
 */
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "login_details")
public class LoginDetails {
    @Id
    @Column(name = "user_name")
    private String userName;
    @Column(name = "user_password")
    private String userPassword;
    @Column(name = "id")
    private String id;
    @Column(name = "name")
    private String name;
    @Column(name = "login_type")
    private String loginType;

    public LoginDetails(String userName, String userPassword, String id, String name, String loginType) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.id = id;
        this.name = name;
        this.loginType = loginType;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLoginType() {
        return loginType;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, JSON_STYLE);
    }
}
