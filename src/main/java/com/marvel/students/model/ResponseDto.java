package com.marvel.students.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 11:53 PM
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDto {

    private final String studentId;
    private final String studentName;
    private String message;

    public ResponseDto(String studentId, String studentName) {
        this.studentId = studentId;
        this.studentName = studentName;
    }

    public ResponseDto(String studentId, String studentName, String message) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getStudentId() {
        return studentId;
    }

    public String getStudentName() {
        return studentName;
    }
}
