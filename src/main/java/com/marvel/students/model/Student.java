package com.marvel.students.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.apache.commons.lang3.builder.ToStringStyle.JSON_STYLE;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 4:55 PM
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "student")
public class Student {
    @Id
    @Column(name = "student_id")
    private final String id;
    private String studentName;
    private String studentNric;
    private String parentName;
    private String parentNric;
    private String contactNumber;
    private String gender;
    private String dob;
    private String email;
    private String district;
    private String country;
    private String address;
    private long postalCode;
    private String userPassword;
    private String interests;
    private String loginType;

    public Student() {
        id = String.valueOf(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this, JSON_STYLE);
    }
}
