package com.marvel.students.repository;

import com.marvel.students.model.LoginDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginDetailsRepository extends CrudRepository<LoginDetails, String> {

}