package com.marvel.students.repository;

import com.marvel.students.model.LoginDetails;
import com.marvel.students.model.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @Author Pradeep Kumar
 * @create 4/9/2021 9:29 PM
 */

@Component
public class StudentManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentManager.class);

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private LoginDetailsRepository loginDetailsRepository;

    @Transactional
    public Student registerStudent(Student student) {
        LOGGER.info("Registering student: [{}, {}]", student.getStudentName(), student.getEmail());
        LoginDetails loginDetails = new LoginDetails(student.getEmail(), student.getUserPassword(), student.getId(), student.getStudentName(), student.getLoginType());
        loginDetailsRepository.save(loginDetails);
        Student save = studentRepository.save(student);
        LOGGER.info("Successfully registered student: [{}, {}]", student.getStudentName(), student.getEmail());
        return student;
    }

    public LoginDetails checkUser(LoginDetails loginDetails) {
        Optional<LoginDetails> dbUserObject = loginDetailsRepository.findById(loginDetails.getUserName());
        LOGGER.info("User object in database: {}", dbUserObject);
        if (dbUserObject.isPresent()) {
            LOGGER.info("dbUserObject.get().getLoginType(): {}", dbUserObject.get().getLoginType());
            LOGGER.info("loginDetails.getLoginType(): {}", loginDetails.getLoginType());
            if(dbUserObject.get().getLoginType().equals(loginDetails.getLoginType())) {
                LoginDetails details = dbUserObject.get();
                return details;
            }
        }
        return null;
    }

    public Student fetchStudent(String studentId) {
        Optional<Student> optionalStudent = studentRepository.findById(studentId);
        if (optionalStudent.isPresent()) {
            LOGGER.info("Student [{}] fetched for Student Id: [{}]", optionalStudent.get(), studentId);
            return optionalStudent.get();
        }
        return null;
    }
}
