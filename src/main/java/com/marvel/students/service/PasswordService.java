package com.marvel.students.service;

import com.marvel.students.config.AWSConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {

    @Autowired
    KMSService kmsService;

    @Autowired
    S3Service s3Service;

    @Autowired
    AWSConfiguration awsConfiguration;


    public String getMarvelDBPassword(){
        byte[] encryptedDBPassword = s3Service.fetchBytesFromBuckets(awsConfiguration.getS3BucketName(),awsConfiguration.getS3DBKey());
       return kmsService.decrypt(encryptedDBPassword);
    }
}
