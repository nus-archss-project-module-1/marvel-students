package com.marvel.students.service;

import com.marvel.students.config.AWSConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;

import javax.annotation.PostConstruct;

@Service
public class S3Service {

    private S3Client s3;
    @Autowired
    AWSConfiguration awsConfiguration;

    @PostConstruct
    public void initialize(){
        AwsBasicCredentials awscred =   AwsBasicCredentials.create(awsConfiguration.getAccesskeyid(),awsConfiguration.getSeceretKeyAccess());
        AwsCredentialsProvider awsCredentialsProvider = StaticCredentialsProvider.create(awscred);
        s3 = S3Client.builder().region(Region.AP_SOUTHEAST_1).credentialsProvider(awsCredentialsProvider).build();
    }


    public byte[] fetchBytesFromBuckets(String bucketName ,String key){
        GetObjectRequest objectRequest = GetObjectRequest.builder()
                .key(key)
                .bucket(bucketName)
                .build();
        ResponseBytes<GetObjectResponse> objectBytes = s3.getObjectAsBytes(objectRequest);
        byte[] data = objectBytes.asByteArray();
        return data;
    }
}
